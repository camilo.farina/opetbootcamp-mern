"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
//configuration the .env file
dotenv_1.default.config();
//Create Express APP
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
//Define the first route of APP. 
app.get('/', (request, response) => {
    //Send Hello World.
    response.send('Welcome to my API Restful: express + TS + SW + MG by: Camilo Farina');
});
//Define the first route of APP. 
app.get('/hello', (request, response) => {
    //Send Hello World.
    response.send('Welcome to my GET route: Hello!');
});
//Execute the app and listen Requests to PORT
app.listen(port, () => console.log(`Express Server Runing at  http://localhost:${port}`));
//# sourceMappingURL=index.js.map