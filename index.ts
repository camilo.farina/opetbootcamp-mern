import express, { Express, Request , Response } from "express";
import dotenv from "dotenv";

//configuration the .env file
dotenv.config();

//Create Express APP
const app: Express = express();
const port:string | number  = process.env.PORT || 8000;

//Define the first route of APP. 
app.get('/',(request,response)=> {
    //Send Hello World.
    response.send('Welcome to my API Restful: express + TS + SW + MG by: Camilo Farina');
});
//Define the first route of APP. 
app.get('/hello',(request,response)=> {
    //Send Hello World.
    response.send('Welcome to my GET route: Hello!');
});


//Execute the app and listen Requests to PORT
app.listen(port, () => console.log(`Express Server Runing at  http://localhost:${port}`)); 