const express = require('express');
const dotenv = require('dotenv');
const { append } = require('express/lib/response');
const { request, response } = require('express');

//configuration the .env file
dotenv.config();

//Create Express APP
const app = express();
const port = process.env.PORT || 8000;


//Define the first route of APP. 
app.get('/',(request,response )=> {
    //Send Hello World.
    response.send('Welcome to my API Restful: express + TS + SW + MG');
});

//Execute the app and listen Requests to PORT
app.listen(port, () => console.log(`Express Server Runing at  http://localhost:${port}`)); 